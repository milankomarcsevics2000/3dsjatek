﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackToMenu : MonoBehaviour
{
    [SerializeField]
    KeyCode keyMenu;

    void Update()
    {
        if (Input.GetKey(keyMenu))
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }
}