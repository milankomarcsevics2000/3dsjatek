﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var x = Input.GetAxisRaw("Horizontal");
        var y = Input.GetAxisRaw("Vertical");
        if (y != 0)
        {
            transform.Translate(
            new Vector3(0, 0, y*2f * Time.deltaTime)
        );
        }
        
        if(x != 0)
        {
            transform.Translate(
                new Vector3(x * 2f * Time.deltaTime, 0, 0)
            );
        }
    }
}
