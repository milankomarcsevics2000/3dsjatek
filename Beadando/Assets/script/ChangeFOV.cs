using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeFOV : MonoBehaviour
{
    [SerializeField]
    KeyCode ChangeFov;
    [SerializeField]
    float FieldOfView = 60;
    
    void Update()
    {
        if(Input.GetKeyDown(ChangeFov))
        {
            if(FieldOfView == 60)
            {
                FieldOfView = 80;
                Camera.main.fieldOfView = FieldOfView;
            }
            else
            {
                FieldOfView = 60;
                Camera.main.fieldOfView = FieldOfView;
            }
        }
    }
}
