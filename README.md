# 3dsJatek

The goal is to navigate the ball through the obstacles and reach the finish without falling down the ledges. The level has two paths the one on the left is quiet easy, but the right one requires serious platforming skills.

Controls:<br>
Movement - Arrow keys<br>
Change camera - C<br>
Restart - R<br>
Exit to main menu - Esc<br>
